﻿# generator-alexa-skill-basic
_Yeoman Generator, alexa-skill-basic, for an Alexa Skill app in Typescript._



This generator creates an absolutely minimal Alexa Skill app skeleton (in typescript).



## Installation

First, install [Yeoman](http://yeoman.io) and [generator-alexa-skill-basic](https://www.npmjs.com/package/generator-alexa-skill-basic) using [npm](https://www.npmjs.com/).
(You'll obviously need [node.js](https://nodejs.org/) installed on your system. Bower is optional.).

```bash
npm install -g yo typescript
npm install -g generator-alexa-skill-basic
```

Then generate your new project:

```bash
mkdir my-ask-project
cd my-ask-project
yo alexa-skill-basic
```

You'll need add `app_id.ts` file with
APP_ID var set to your app id.



## Examples

Alexa skills initially scaffolded using the Alexa-Skill-Basic generator.

* [Match Game](https://www.amazon.com/Sideway-Bot-Match-Game/dp/B075TWLVVP/)
* [Solo Blackjack](https://www.amazon.com/Sideway-Bot-Solo-Blackjack/dp/B07583PP51/)
* [Smart Restaurant](https://www.amazon.com/dp/B0763L5GP1/)
* [Python Master](https://www.amazon.com/Sideway-Bot-Python-Master/dp/B073T35BZ4/)




## License

MIT © [Harry Y](https://gitlab.com/yofactory)



