import * as Alexa from 'alexa-sdk';
import { APP_ID } from './app-id';


var SKILL_NAME = "<%= appName %>";

var WELCOME_MESSAGE = `Welcome to ${SKILL_NAME}! What would you like to do? `;
var WELCOME_REPROMPT = `You can ask me to start conversation. Or, for more information, say 'help me'.  What would you like to do now? `;
var EXIT_SKILL_MESSAGE = `Thank you for using ${SKILL_NAME}!  Bye! `;
var HELP_MESSAGE = `${SKILL_NAME} is an Alexa Skill for a simple conversation. `
var HELP_REPROMPT = `You can start by asking me to start to start conversation. Now, tell me what you'd like to do. `;


var states = {
  START: "_START",
  CONVO: "_CONVO"
};


export const handlers: Alexa.Handlers = {
  "LaunchRequest": function () {
    this.handler.state = states.START;
    this.emitWithState("Start");
  },
  "StartConvoIntent": function () {
    this.handler.state = states.START;
    this.emitWithState("StartConvoIntent");
  },
  "AMAZON.HelpIntent": function () {
    this.emit(":ask", HELP_MESSAGE, HELP_REPROMPT);
  },
  "Unhandled": function () {
    this.handler.state = states.START;
    this.emitWithState("Start");
  }
};

export var startHandlers: Alexa.Handlers = Alexa.CreateStateHandler(states.START, {
  "Start": function () {
    this.emit(":ask", WELCOME_MESSAGE, WELCOME_REPROMPT);
  },
  "StartConvoIntent": function () {
    this.handler.state = states.CONVO;
    this.emitWithState("StartConvo");
  },
  "AMAZON.StopIntent": function () {
    this.emit(":tell", EXIT_SKILL_MESSAGE);
  },
  "AMAZON.CancelIntent": function () {
    this.emit(":tell", EXIT_SKILL_MESSAGE);
  },
  "AMAZON.HelpIntent": function () {
    this.emit(":ask", HELP_MESSAGE, HELP_REPROMPT);
  },
  "Amazon.ResumeIntent": function () {
    this.handler.state = states.CONVO;
    this.emitWithState("StartConvo");
  },
  "Unhandled": function () {
    console.log(">>> START::Unhandled");

    this.emitWithState("Start");
  }
});


export var convoHandlers: Alexa.Handlers = Alexa.CreateStateHandler(states.CONVO, {
  "StartConvoIntent": function () {
    this.emitWithState("StartConvo");
  },
  "StartConvo": function () {
    this.attributes["response"] = "";
    this.emitWithState("AskQuestion");
  },
  "AskQuestion": function () {
    console.log(">>> CONVO::AskQuestion");

    let response = this.attributes["response"];
    this.attributes["response"] = "";
    let question = "How are you doing? ";
    let repromptQ = "What are you doing? ";
    let speechOutput = response + " <break time='500ms'/> " + question;
    this.emit(":ask", speechOutput, repromptQ);
  },
  "AnswerResponseIntent": function () {
    console.log(">>> CONVO::AnswerResponseIntent");

    let response = " OK. I got it. ";
    this.attributes["response"] = response;
    this.emitWithState("AskQuestion");
  },

  "AMAZON.StartOverIntent": function () {
    this.emitWithState("StartConvo");
  },

  "AMAZON.NextIntent": function () {
    this.emitWithState("StartConvo");
  },
  "AMAZON.YesIntent": function () {
    this.emitWithState("StartConvo");
  },
  "AMAZON.NoIntent": function () {
    this.emit(":tell", EXIT_SKILL_MESSAGE);
  },

  "AMAZON.StopIntent": function () {
    this.emit(":tell", EXIT_SKILL_MESSAGE);
  },
  "AMAZON.CancelIntent": function () {
    this.emit(":tell", EXIT_SKILL_MESSAGE);
  },
  "AMAZON.HelpIntent": function () {
    this.emit(":ask", HELP_MESSAGE, HELP_REPROMPT);
  },
  "Amazon.ResumeIntent": function () {
    this.emitWithState("AskQuestion");
  },

  "Unhandled": function () {
    console.log(">>> CONVO::Unhandled");

    let error = " I did not understand. Please try again. ";
    this.attributes["response"] = error;
    this.emitWithState("AskQuestion");
  }
});


// Main handler.
export default function handler(event, context, callback) {
  console.log('<%= appName %> main handler loaded....');

  var alexa: Alexa.AlexaObject = Alexa.handler(event, context, callback);
  alexa.appId = APP_ID;
  alexa.registerHandlers(handlers, startHandlers, convoHandlers);
  alexa.execute();
}


