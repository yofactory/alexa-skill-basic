# generator-alexa-skill-basic [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> This generator creates an absolutely minimal Alexa Skill app skeleton in typescript.

## Installation

First, install [Yeoman](http://yeoman.io) and generator-alexa-skill-basic using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo typescript
npm install -g generator-alexa-skill-basic
```

Then generate your new project:

```bash
yo alexa-skill-basic
```

You'll need to add `app-id.ts` in the src directory,
with `APP_ID` variable.



## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

MIT © [Harry Y](https://gitlab.com/realharry)


[npm-image]: https://badge.fury.io/js/generator-alexa-skill-basic.svg
[npm-url]: https://npmjs.org/package/generator-alexa-skill-basic
[travis-image]: https://travis-ci.org/harrywye/generator-alexa-skill-basic.svg?branch=master
[travis-url]: https://travis-ci.org/harrywye/generator-alexa-skill-basic
[daviddm-image]: https://david-dm.org/harrywye/generator-alexa-skill-basic.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/harrywye/generator-alexa-skill-basic
